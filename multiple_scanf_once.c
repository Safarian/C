#include <stdio.h>


int main()
{
    int intX;
    char cha;
    float floatY;
    
    printf("give a integer, character, and a float: ");
    scanf("%d%c%f", &intX, &cha, &floatY);
    printf("\nI:%d C:%c F:%f \n", intX, cha, floatY);

    return 0;
}
